# -*- coding:utf-8 -*-
import cv2
import os
import sys
import numpy as np
from math import *

# 制作模板的图像路径
ORI_IMAGE_PATH = r'1.jpg'
# # 待检测图像的路径
# DETECT_IMAGE_PATH = r'mm11.jpg'


def rotateImage(img,degree,pt1,pt2,pt3,pt4):
    height,width=img.shape[:2]
    heightNew = int(width * fabs(sin(radians(degree))) + height * fabs(cos(radians(degree))))
    widthNew = int(height * fabs(sin(radians(degree))) + width * fabs(cos(radians(degree))))
    matRotation=cv2.getRotationMatrix2D((width/2,height/2),degree,1)
    matRotation[0, 2] += (widthNew - width) / 2
    matRotation[1, 2] += (heightNew - height) / 2
    imgRotation = cv2.warpAffine(img, matRotation, (widthNew, heightNew), borderValue=(255, 255, 255))
    pt1 = list(pt1)
    pt3 = list(pt3)
    [[pt1[0]], [pt1[1]]] = np.dot(matRotation, np.array([[pt1[0]], [pt1[1]], [1]]))
    [[pt3[0]], [pt3[1]]] = np.dot(matRotation, np.array([[pt3[0]], [pt3[1]], [1]]))
    imgOut=imgRotation[int(pt1[1]):int(pt3[1]),int(pt1[0]):int(pt3[0])]
    cv2.imshow("imgOut",imgOut) #裁减得到的旋转矩形框
    cv2.imwrite("imgOut.jpg",imgOut)

    return imgRotation



# 读取制作模板和待检测的原图
kernel = np.ones((3, 3), np.uint8)
img_model = cv2.imread(ORI_IMAGE_PATH)
img_model_copy = img_model.copy()
# 灰度化并保存
gray_img_model = cv2.cvtColor(img_model, cv2.COLOR_BGR2GRAY)
ret, gray_img_model_thresh = cv2.threshold(
    gray_img_model, 150, 255, cv2.THRESH_BINARY_INV)  # 二值化
image_contours_mask = cv2.erode(gray_img_model_thresh, kernel, iterations=1)
cv2.imshow("img_model", gray_img_model)
cv2.waitKey(0)
image_contours, det_image, hierarchy = cv2.findContours(
    image_contours_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)  # 找方框
area = []
for k in range(len(det_image)):
    det_img_new = cv2.drawContours(img_model_copy, det_image[k], -1, (0, 0, 255), 3)
    area.append(cv2.contourArea(det_image[k]))
print(area)
max_area_contours = np.argmax(area)
print(max_area_contours)
rect = cv2.minAreaRect(det_image[max_area_contours])

center, (h, w), degree = rect  # （中心(x,y), (宽,高), 旋转角度）
box = cv2.boxPoints(rect)  # 获取最小外接矩形的4个顶点坐标
box = np.int0(box)
print(box)
cv2.drawContours(img_model_copy, [box], 0, (0, 0, 255), 5)
rotateImage(img_model, -degrees(atan2(abs(box[0][1]-box[1][1]),
                                    abs(box[0][0]-box[1][0]))), box[0], box[1], box[2], box[3])
cv2.imshow("img_model", img_model_copy)
cv2.waitKey(0)

#
# if __name__ == '__main__':
#     # main()中主要做了模板匹配的操作，模板相减得到污染物位置没有做
#     cap = cv2.VideoCapture(1)
#     cap.set(15, -10);
#     secuess, fream = cap.read()
#     main(fream)
