# -*- coding:utf-8 -*-
import cv2
import os
import sys
import numpy as np
from math import *

# 制作模板的图像路径
ORI_IMAGE_PATH = r'1.jpg'
# # 待检测图像的路径
# DETECT_IMAGE_PATH = r'mm11.jpg'


def rotateImage(img,degree,pt1,pt2,pt3,pt4):
    height,width=img.shape[:2]
    heightNew = int(width * fabs(sin(radians(degree))) + height * fabs(cos(radians(degree))))
    widthNew = int(height * fabs(sin(radians(degree))) + width * fabs(cos(radians(degree))))
    matRotation=cv2.getRotationMatrix2D((width/2,height/2),degree,1)
    matRotation[0, 2] += (widthNew - width) / 2
    matRotation[1, 2] += (heightNew - height) / 2
    imgRotation = cv2.warpAffine(img, matRotation, (widthNew, heightNew), borderValue=(255, 255, 255))
    pt1 = list(pt1)
    pt3 = list(pt3)
    [[pt1[0]], [pt1[1]]] = np.dot(matRotation, np.array([[pt1[0]], [pt1[1]], [1]]))
    [[pt3[0]], [pt3[1]]] = np.dot(matRotation, np.array([[pt3[0]], [pt3[1]], [1]]))
    imgOut=imgRotation[int(pt1[1]):int(pt3[1]),int(pt1[0]):int(pt3[0])]
    #cv2.imshow("imgOut",imgOut) #裁减得到的旋转矩形框
    cv2.imwrite("imgOut.jpg",imgOut)

    return imgOut


def main(fream):
    # 读取制作模板和待检测的原图
    kernel = np.ones((3, 3), np.uint8)
    img_model = cv2.imread(ORI_IMAGE_PATH)
    img_model_copy = img_model.copy()
    fream_copy = fream.copy()
    # 灰度化并保存
    gray_img_model = cv2.cvtColor(img_model, cv2.COLOR_BGR2GRAY)
    gray_img_fream = cv2.cvtColor(fream, cv2.COLOR_BGR2GRAY)
    ret, gray_img_model_thresh = cv2.threshold(
        gray_img_model, 160, 255, cv2.THRESH_BINARY_INV)  # 二值化    原数值   150   255

    ret_, gray_img_model_thresh_fream = cv2.threshold(
        gray_img_fream, 160, 255, cv2.THRESH_BINARY_INV)  # 二值化
    cv2.imshow("s",gray_img_model_thresh_fream)
    image_contours_mask = cv2.erode(gray_img_model_thresh, kernel, iterations=1)
    image_contours_mask_fream = cv2.erode(gray_img_model_thresh_fream, kernel, iterations=1)
    image_contours_mask = cv2.dilate(image_contours_mask, np.ones((7, 7), np.uint8), iterations=1)
    image_contours_mask_fream = cv2.dilate(image_contours_mask_fream, np.ones((7, 7), np.uint8), iterations=1)
    cv2.imshow("ss", image_contours_mask_fream)
    cv2.imshow("ww", image_contours_mask)

    #cv2.imshow("img_model", gray_img_model)

    #cv2.waitKey(0)
    image_contours, det_image, hierarchy = cv2.findContours(
        image_contours_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)  # 找方框
    image_contours_fream, det_image_fream, hierarchy_ = cv2.findContours(
        image_contours_mask_fream, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)  # 找方框
    if len(det_image_fream) != 0:
        area = []
        area_fream = []
        for k in range(len(det_image)):
            det_img_new = cv2.drawContours(img_model_copy, det_image[k], -1, (0, 0, 255), 3)
            area.append(cv2.contourArea(det_image[k]))
        for k in range(len(det_image_fream)):
            det_img_new_ = cv2.drawContours(fream_copy, det_image_fream[k], -1, (0, 0, 255), 3)
            area_fream.append(cv2.contourArea(det_image_fream[k]))
        #print(area)
        max_area_contours = np.argmax(area)
        max_area_contours_fream = np.argmax(area_fream)
        #print(max_area_contours)
        rect = cv2.minAreaRect(det_image[max_area_contours])
        rect_fream = cv2.minAreaRect(det_image_fream[max_area_contours_fream])

        center, (h, w), degree = rect  # （中心(x,y), (宽,高), 旋转角度）
        center_fream, (h_fream, w_fream), degree_fream = rect_fream  # （中心(x,y), (宽,高), 旋转角度）
        box = cv2.boxPoints(rect)  # 获取最小外接矩形的4个顶点坐标
        box = np.int0(box)
        box_fream = cv2.boxPoints(rect_fream)  # 获取最小外接矩形的4个顶点坐标
        box_fream = np.int0(box_fream)
        #print(box)
        cv2.drawContours(img_model_copy, [box], 0, (0, 0, 255), 5)
        cv2.drawContours(fream_copy, [box_fream], 0, (0, 0, 255), 5)
        imgOut = rotateImage(img_model, -degrees(atan2(abs(box[0][1]-box[1][1]),
                                            abs(box[0][0]-box[1][0]))), box[0], box[1], box[2], box[3])
        imgOut_fream = rotateImage(fream, -degrees(atan2(abs(box_fream[0][1] - box_fream[1][1]),
                                              abs(box_fream[0][0] - box_fream[1][0]))), box_fream[0], box_fream[1], box_fream[2], box_fream[3])
        if imgOut_fream.shape[0] < imgOut_fream.shape[1]:
            imgOut_fream = cv2.rotate(imgOut_fream, cv2.cv2.ROTATE_90_CLOCKWISE)
        high_imgOut,wigth_imgOut,_ = imgOut.shape
        imgOut_fream = cv2.resize(imgOut_fream, (wigth_imgOut, high_imgOut))

        imgOut_gray = cv2.cvtColor(imgOut, cv2.COLOR_BGR2GRAY)
        imgOut_fream_gray = cv2.cvtColor(imgOut_fream, cv2.COLOR_BGR2GRAY)
        _, imgOut_gray = cv2.threshold(
            imgOut_gray, 120, 255, cv2.THRESH_BINARY)  # 二值化
        _, imgOut_fream_gray = cv2.threshold(
            imgOut_fream_gray, 120, 255, cv2.THRESH_BINARY)  # 二值化
        result = imgOut_gray-imgOut_fream_gray
        result = cv2.erode(result, np.ones((7, 7), np.uint8), iterations=1)
        result = cv2.dilate(result, np.ones((7, 7), np.uint8), iterations=1)
        # _, result = cv2.threshold(
        #     result, 200, 255, cv2.THRESH_BINARY)  # 二值化
        image_contours, det_image_result, hierarchy = cv2.findContours(
            result, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)  # 找方框
        if len(det_image_result) != 0:
            for k in range(len(det_image_result)):
                if cv2.contourArea(det_image_result[k]) > 40:
                    det_img_new = cv2.drawContours(imgOut_fream, det_image_result[k], -1, (0, 0, 255), 3)
        cv2.imshow("result", result)  # 裁减得到的旋转矩形框
        cv2.imshow("imgOut", imgOut)  # 裁减得到的旋转矩形框
        cv2.imshow("imgOut_fream_gray", imgOut_fream_gray)  # 裁减得到的旋转矩形框
        cv2.imshow("1111", fream_copy)
        cv2.imshow("imgOut_fream", imgOut_fream)

    cv2.imshow("img_", fream)
    #cv2.waitKey(2)


if __name__ == '__main__':
    # main()中主要做了模板匹配的操作，模板相减得到污染物位置没有做
    cap = cv2.VideoCapture(1)
    cap.set(15, -11)
    # cap.set(3, 1280)
    # cap.set(4, 1024)
    while True:
        secuess, fream1 = cap.read()
        # cv2.imshow("img_", fream)
        main(fream1)
        key = cv2.waitKey(10)
        if key == 27:
            break
