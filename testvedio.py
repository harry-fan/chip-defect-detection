# -*- coding: utf-8 -*-
"""
Created on Fri Apr  2 13:47:29 2021

@author: 10204
"""


import cv2
import sys
#选择摄像头号，一般从 0 开始
cap = cv2.VideoCapture(1)
file = r"img/"
#先设置参数，然后读取参数
# cap.set(3,1280)
# cap.set(4,1024)
# cap.set(15, 0.1)
print("width={}".format(cap.get(3)))
print("height={}".format(cap.get(4)))
print("exposure={}".format(cap.get(15)))

i = 1
num = 196

while True:
    ret, img = cap.read()
    cv2.imshow("input", img)
    i = i+1
    if i % 20 == 0:
        # cv2.imwrite(str("train_data/0/")+str(num)+str(".jpg"), img)
        num = num+1
# 按 ESC 键退出
    key = cv2.waitKey(10)
    if key == 27:
        break
