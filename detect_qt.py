import sys
import test
from display import Display
from PyQt5.QtWidgets import QApplication, QMainWindow

if __name__ =='__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()
    ui = test.Ui_MainWindow()
    ui.setupUi(MainWindow)
    display = Display(ui, MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())